This project publishes plain HTML site using GitLab Pages

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

This repository will be pushed to a mirror repository at 
https://gitlab.com/m1limited/test-reports which will publish it's content to
GitLab Pages hosted on gitlab.io domain.
